package com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "superhero")
public class Superhero {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(
            nullable = false,
            name = "superheroname",
            length = 100
    )
    private String superheroName;


    @Column(
            length = 200
    )
    private String secretIdentity;

    @ManyToMany(
            fetch = FetchType.LAZY
    )
    @JoinTable(
            name = "superhero_powers",
            joinColumns = @JoinColumn(name = "superhero_id"),
            inverseJoinColumns = @JoinColumn(name = "power_id")
    )
    @ToString.Exclude
    private List<Power> powers;

    @ManyToMany(
            fetch = FetchType.LAZY
    )
    @ToString.Exclude
    private List<Villain> villains;

    @OneToOne
    private Villain nemesis;

    @OneToMany(
            mappedBy = "mentor",
            fetch = FetchType.LAZY
    )
    @ToString.Exclude
    private List<Superhero> sidekicks;

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @ToString.Exclude
    private Superhero mentor;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Superhero superhero = (Superhero) o;
        return id != null && Objects.equals(id, superhero.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
