package com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "power")
public class Power {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String description;
    @ManyToMany(mappedBy = "powers")
    @ToString.Exclude
    private List<Superhero> superheroes;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Power power = (Power) o;
        return id != null && Objects.equals(id, power.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
