package com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.api;


import com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.api.dto.SuperheroDTO;
import com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.model.Superhero;
import com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.service.SuperheroService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/superheroes", produces = APPLICATION_JSON_VALUE)
public class SuperheroController {

    private final SuperheroService superheroService;

    @GetMapping
    ResponseEntity<List<SuperheroDTO>> searchSuperhero(){
        List<Superhero> list = superheroService.getAllSuperheroes();

        List<SuperheroDTO> listDTO = mapToDTO(list);

        return ResponseEntity.ok(listDTO);
    }

    private List<SuperheroDTO> mapToDTO(List<Superhero> list){
        return list
                .stream()
                .map(this::mapToDTO)
                .toList();
    }

    private SuperheroDTO mapToDTO(Superhero superhero){
        return new SuperheroDTO(
                superhero.getId(),
                superhero.getSuperheroName(),
                superhero.getSecretIdentity(),
                superhero.getPowers().size()
        );
    }
}