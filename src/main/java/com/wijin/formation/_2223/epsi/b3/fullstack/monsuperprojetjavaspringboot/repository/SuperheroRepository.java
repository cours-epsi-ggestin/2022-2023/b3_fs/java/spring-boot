package com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.repository;

import com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.model.Superhero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperheroRepository extends JpaRepository<Superhero, Long> {
}
