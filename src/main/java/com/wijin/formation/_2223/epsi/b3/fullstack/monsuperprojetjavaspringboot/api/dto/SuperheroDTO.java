package com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.api.dto;

public record SuperheroDTO(
        Long id,
        String superheroName,
        String secretIdentity,
        Integer nbPower
) {
}
