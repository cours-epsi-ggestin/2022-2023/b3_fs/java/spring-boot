package com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonSuperProjetJavaSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonSuperProjetJavaSpringBootApplication.class, args);
    }

}
