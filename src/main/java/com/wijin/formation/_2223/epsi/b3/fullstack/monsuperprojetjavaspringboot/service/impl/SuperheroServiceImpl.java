package com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.service.impl;

import com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.model.Superhero;
import com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.repository.SuperheroRepository;
import com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.service.SuperheroService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SuperheroServiceImpl implements SuperheroService {

    private final SuperheroRepository superheroRepository;

    @Override
    public List<Superhero> getAllSuperheroes() {
        return superheroRepository.findAll();
    }
}
