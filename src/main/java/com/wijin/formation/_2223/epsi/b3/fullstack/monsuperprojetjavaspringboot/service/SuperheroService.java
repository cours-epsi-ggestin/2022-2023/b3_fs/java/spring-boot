package com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.service;

import com.wijin.formation._2223.epsi.b3.fullstack.monsuperprojetjavaspringboot.model.Superhero;

import java.util.List;

public interface SuperheroService {
    List<Superhero> getAllSuperheroes();
}
